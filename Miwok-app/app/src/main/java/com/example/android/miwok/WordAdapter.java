package com.example.android.miwok;

import android.app.Activity;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class WordAdapter extends ArrayAdapter<Word>{

    private int mColorResourceId;



    public WordAdapter(Activity context, ArrayList<Word> words, int colorResourceId) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, words);
        mColorResourceId = colorResourceId;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Get the {@link Word} object located at this position in the list
//        Setting a word object, naming it and getting its posiotion from the ArrayList we made in numbers activity
//        this uses the super class arrayadapter method (getItem(position)
        Word currentWord = getItem(position);

//        ImageView miwokImageView = (ImageView) listItemView.findViewById(R.id.image_view)

        // Find the TextView in the list_item.xml layout with the ID version_name
//        Defining a textview, naming it and casting it from an id set in the Word object
        TextView miwokTextView = (TextView) listItemView.findViewById(R.id.miwok_text_view);

        // setting the text of our new textview to the text of the current ArrayList position text using the custom method in
//        the word object (getmiwoktranslation)
        miwokTextView.setText(currentWord.getMiwokTranslation());
//        miwokImageView.setImageDrawable(currentWord.);

        // Find the TextView in the list_item.xml layout with the ID version_number
        TextView defaultTextView = (TextView) listItemView.findViewById(R.id.default_text_view);
        // Get the version number from the current AndroidFlavor object and
        // set this text on the number TextView
        defaultTextView.setText(currentWord.getDefaultTranslation());

        View textContainerView = listItemView.findViewById((R.id.text_container));

        int color = ContextCompat.getColor(getContext(),mColorResourceId);

        textContainerView.setBackgroundColor(color);


        ImageView imageResource = (ImageView) listItemView.findViewById(R.id.image_view);

        if (currentWord.hasImage()) {
            imageResource.setImageResource(currentWord.getImageResourceId());
            imageResource.setVisibility(View.VISIBLE);
        }else{
            imageResource.setVisibility(View.GONE);
        }





        // Return the whole list item layout (containing 2 TextViews and an ImageView)
        // so that it can be shown in the ListView
        return listItemView;
    }
}